package com.training.emp.repository;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.training.emp.model.Employee;

public class EmployeeRepositoryImpl implements EmployeeRepository {

	// dummy database
	private static Set<Employee> employeeSet;

	public EmployeeRepositoryImpl() {
		employeeSet = new HashSet<>();
		Employee employee1 = new Employee(101, "Immanuel" , "Immanuel@email.com" );
		Employee employee2 = new Employee(102, "Saranya" , "Saranya@gmail.com");
		Employee employee3 = new Employee(103, "Prithivi" , "Prithivi@gmail.com");
		employee1.setEmail("hifi@gmail.com");
		employeeSet.add(employee1);
		employeeSet.add(employee2);
		employeeSet.add(employee3);

	}

	@Override
	public Set<Employee> findAll() {
		return employeeSet;

	}

	@Override
	public Employee findById(int id) {
		for (Employee employee : employeeSet) {
			if (employee.getId() == id) {
				return employee;
			}
		}
		return null;
	}

	@Override
	public void save(Employee employee) {
		employeeSet.add(employee);
	}

	@Override
	public void update(Employee employee) {
		// emp - exisiting emp
		// employee - new data to be updated
		for (Employee emp : employeeSet) {
			if (emp.getId() == employee.getId()) {
				emp.setName(employee.getName());
			}
		}

	}

	@Override
	public void delete(int id) {
		Iterator<Employee> it = employeeSet.iterator();
		while (it.hasNext()) {
			if (it.next().getId() == id) {
				it.remove();
			}
		}

	}

	@Override
	public Employee findByName(String str) {
		for (Employee employee : employeeSet) {
			if (employee.getName().equals(str)) {
				return employee;
			}
		}
		return null;
	}

	@Override
	public Employee search(String str) {
		return null;
	}

}
